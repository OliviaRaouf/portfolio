$(document).ready(function () {
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;

    trigger.click(function () {
        hamburger_cross();      
    });

    function hamburger_cross() {

        if (isClosed == true) {          
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {   
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    }); 
    $('.flip').click(function(){
        $(this).toggleClass('flipped');
    });

    var item_num = $('.entertain-div-wrap nav li').length + 1;
    var btn_state = true;

    $('.entertain-div-wrap nav li').hover(function(){
        $(this).addClass('hover');
    },function(){
        $(this).removeClass('hover');
    });

    $('.entertain-div-wrap nav li').on('click',function(){
        if(btn_state){
            btn_state = !btn_state;
            $('.entertain-div-wrap nav li').removeClass('currentPage');
            $(this).addClass('currentPage');

            var i = $('.entertain-div-wrap nav li').index(this);
            $('article').removeClass('show');
            //            $('article').addClass('hide');
            $('article').eq(i).addClass('show');

            setTimeout(function(){
                btn_state = !btn_state;
            },500);
        }
    });
    $('.one').click(function(){
        $('.collapse')
            .collapse('hide');
    });
    $('.openall').click(function(){
        $('.collapse:not(".in")')
            .collapse('show');
    });
});

