$(document).ready(function() {
  var getslideHeight = $('.slide.active').height();

  $('.slides').css({
    height: getslideHeight
  });

  function calcslideHeight() {
    getslideHeight = $('.slide.active').height();

    $('.slides').css({
      height: getslideHeight
    });
  }

  function animateContentColor() {
    var getslideColor = $('.slide.active').attr('slide-color');

//    $('body').css({
//      background: getslideColor
//    });

//    $('.title').css({
//      color: getslideColor
//    });

//    $('.btn').css({
//      color: getslideColor
//    });
  }

  var slideItem = $('.slide'),
    slideCurrentItem = slideItem.filter('.active');

  $('#next').on('click', function(e) {
    e.preventDefault();

    var nextItem = slideCurrentItem.next();

    slideCurrentItem.removeClass('active');

    if (nextItem.length) {

      slideCurrentItem = nextItem.addClass('active');
    } else {
      slideCurrentItem = slideItem.first().addClass('active');
    }

    calcslideHeight();
    animateContentColor();
  });

  $('#prev').on('click', function(e) {
    e.preventDefault();

    var prevItem = slideCurrentItem.prev();

    slideCurrentItem.removeClass('active');

    if (prevItem.length) {
      slideCurrentItem = prevItem.addClass('active');
    } else {
      slideCurrentItem = slideItem.last().addClass('active');
    }

    calcslideHeight();
    animateContentColor();
  });

  // Ripple
  $('[ripple]').on('click', function(e) {
    var rippleDiv = $('<div class="ripple" />'),
      rippleSize = 60,
      rippleOffset = $(this).offset(),
      rippleY = e.pageY - rippleOffset.top,
      rippleX = e.pageX - rippleOffset.left,
      ripple = $('.ripple');

    rippleDiv.css({
      top: rippleY - (rippleSize / 2),
      left: rippleX - (rippleSize / 2),
      background: $(this).attr("ripple-color")
    }).appendTo($(this));

    window.setTimeout(function() {
      rippleDiv.remove();
    }, 1900);
  });
});


//
//--------------------clock---------------------------------


var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function checkTime() {
  var date = new Date();
  var sufix = '';
  var hours = ('0' + date.getHours()).slice(-2);
  var minutes = ('0' + date.getMinutes()).slice(-2);
  var day = date.getDate();
  var month = monthNames[date.getMonth()];
  var weekday = dayNames[date.getDay()];
  if (day > 3 && day < 21) sufix = 'th';
  switch (day % 10) {
    case 1:
      sufix = "st";
    case 2:
      sufix = "nd";
    case 3:
      sufix = "rd";
    default:
      sufix = "th";
  }
  document.getElementById('time').innerHTML = "  It's <span class='hour'>" + hours + ":" + minutes + "</span><br/><span class='date'>" + month + ' ' + day + sufix + ', ' + weekday + '.';
}

setInterval(checkTime(), 1000);


