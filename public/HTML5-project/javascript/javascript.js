var pic=document.getElementById("pic");
var arrow=document.getElementById("arrow");
var div=document.getElementById("hidden");
pic.addEventListener("click", function() {
        
    if(div.style.display=="block"){  
        div.style.display="none";
         }
    else{
        div.style.display="block";
         }
    
});
arrow.addEventListener("click", function() {
    
         if(div.style.display=="block"){  
        div.style.display="none";
         }
    else{
        div.style.display="block";
         }
    
});



  // Video
  var video = document.getElementById("video");

  // Buttons
  var playButton = document.getElementById("play-pause");
  var muteButton = document.getElementById("mute");
  var fullScreenButton = document.getElementById("full-screen");

  // Sliders
  var seekBar = document.getElementById("seek-bar");
  var volumeBar = document.getElementById("volume-bar");

// Event listener for the play/pause button
playButton.addEventListener("click", function() {
  if (video.paused == true) {
    // Play the video
    video.play();

    // Update the button text to 'Pause'
    playButton.innerHTML = "Pause";
  } else {
    // Pause the video
    video.pause();

    // Update the button text to 'Play'
    playButton.innerHTML = "Play";
  }
});
                
                // Event listener for the mute button
muteButton.addEventListener("click", function() {
  if (video.muted == false) {
    // Mute the video
    video.muted = true;

    // Update the button text
    muteButton.innerHTML = "Unmute";
  } else {
    // Unmute the video
    video.muted = false;

    // Update the button text
    muteButton.innerHTML = "Mute";
  }
    });
    // Event listener for the full-screen button
fullScreenButton.addEventListener("click", function() {
  if (video.requestFullscreen) {
    video.requestFullscreen();
  } else if (video.mozRequestFullScreen) {
    video.mozRequestFullScreen(); // Firefox
  } else if (video.webkitRequestFullscreen) {
    video.webkitRequestFullscreen(); // Chrome and Safari
  }
});
    
    // Event listener for the seek bar
seekBar.addEventListener("change", function() {
  // Calculate the new time
  var time = video.duration * (seekBar.value / 100);

  // Update the video time
  video.currentTime = time;
});
    
    // Update the seek bar as the video plays
video.addEventListener("timeupdate", function() {
  // Calculate the slider value
  var value = (100 / video.duration) * video.currentTime;

  // Update the slider value
  seekBar.value = value;
});
    
    // Pause the video when the slider handle is being dragged
seekBar.addEventListener("mousedown", function() {
  video.pause();
});

// Play the video when the slider handle is dropped
seekBar.addEventListener("mouseup", function() {
  video.play();
});
    
    // Event listener for the volume bar
volumeBar.addEventListener("change", function() {
  // Update the video volume
  video.volume = volumeBar.value;
});



window.onscroll = function() {myFunction()};

function myFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("mock").className = "animate1";
    } else {
        document.getElementById("mock").className = "";
    }
      if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
     document.getElementById("left-position").className = "animate2 sub";
             document.getElementById("right-position").className = "animate3 sub";
    } else {
        document.getElementById("left-position").className = "";
        document.getElementById("right-position").className = "sub";
}
 
}


function dragstart(e) {
  // e.preventDefault();
  e.dataTransfer.setData('data',e.target.id)
  console.log('drag started');
}
function dragover(e){
  e.preventDefault();
  console.log('drag over!');
}
function drop(e){
 e.preventDefault();
var id=  e.dataTransfer.getData('data');
e.target.src=document.getElementById(id).src;
 var paragraph=document.getElementById("par");
var name=document.getElementById("sp");   
if(id=='pic1'){
    
     paragraph.innerHTML = '"Good design at the front-end suggests that everything is in order at the back-end, whether or not that is the case."';
     name.innerHTML = "Dmitry Fadeyev";
}
    if(id=='pic2'){
    
     paragraph.innerHTML = '"A posuere donec senectus suspendisse bibendum magna ridiculus a justo orci parturient suspendisse ad rhoncus "';
     name.innerHTML = "John";
}
    if(id=='pic3'){
    
      paragraph.innerHTML = '"consectetur scelerisque integer suspendisse a mus integer elit massa ut."'
      name.innerHTML = "Mark ";
}
    if(id=='pic4'){
    
      paragraph.innerHTML = '"cursus ut parturient viverra elit aliquam ultrices est sem. Tellus nam ad fermentum ac enim est duis facilis"';
       name.innerHTML = "Samy";
}
    if(id=='pic5'){
    
      paragraph.innerHTML = '"ultrices est sem. Tellus nam ad fermentum"';
       name.innerHTML = "mmmmmmm";
}
}
